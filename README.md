# Pulse

Pulse is a microblogging platform that's easy-to-install, configure, and customize. It's built from the ground up to support ActivityPub, the primary protocol of the Fediverse. Like other microblogging platforms on the Fediverse, it allows users to share, boost, create, like, and comment. Built with PHP and running MariaDB or MySQL on the backend, it's also easy to deploy in a production environment.
